# sawyer_gail
This repository contains the simulation source codes and examples for applying machine learning algorithms on Sawyer Robot in Gazebo simulation environment.
## Dependencies & Packages:
- Ubuntu 16.04
- ROS Kinetic 
- Gazebo 7
- Python 2.7

### Note: Create a catkin workspace (like sawyer_ws) for adding all the Sawyer robot ROS packages, SDK and Novint Falcon drivers mentioned below.

- Sawyer-ROS Packages & SDK [https://github.com/RethinkRobotics]: includes Sawyer robot model, simulator packages & Intera SDK for accessing the Sawyer robot in Gazebo environment.
- libnifalcon [https://github.com/libnifalcon/libnifalcon]: Open source drivers for the Novint Falcon haptic controller
- ros-falcon [https://github.com/Ashkan372/ROS-Falcon]: Novint Falcon ROS packages 

### File setup:

- sawyer_gazebo_env folder contains the 3D models of table, peg, hole, etc for spawing in the Gazebo simulator.
- sawyer_demonstration folder contains the python script for launching the sawyer robot in Gazebo & also provision for recording the data.


## How to access the files in the package:

### Terminal 1: To launch the Sawyer robot in Gazebo environment - for peg in hole. 
For spawning the Sawyer robot in the Gazebo simulator along with a table and hole.

$ cd sawyer_ws/

$ ./intera.sh sim 

$ roslaunch sawyer_demonstration sawyer_demonstration.launch 


### Terminal 2: For activating the novint falcon interface,

$ cd sawyer_ws/

$ ./intera.sh sim

$ cd devel/lib/ros_falcon/

$ ./joystick


### Terminal 3: For recording the images and input states from the simulation env. 
This script helps with the recording of the input states data for training the networks.

$ cd sawyer_ws/src/sawyer-robot-learning/sawyer_demonstraton/scripts/

$ python recorder.py 

***Note:***  Click the ***right button*** to start the recording of data, then ***center button*** for manoeuvring the arm in simulation and ***upper button*** for pushing the peg down inside the hole.   

## To run GAIL:
### Terminal 1: launch the Sawyer robot in Gazebo environment - for peg in hole - to run GAIL code. 

$ cd sawyer_ws/

$ ./intera.sh sim 

$ roslaunch sawyer_demonstration sawyer_pih.launch

***Note:***  Refer sawyer_gail/sawyer_demonstration/scripts/gail_rnn/Readme.md for running GAIL code, in separate terminal.
