import argparse
import os
import itertools
import sys
import pickle
import math
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from itertools import count
from util import *
import csv
import cv2


dtype = torch.float
torch.set_default_dtype(dtype)

parser = argparse.ArgumentParser(description='Save expert trajectory')
parser.add_argument('--data-path', metavar='G',
                    help='name of the expert model')
parser.add_argument('--out-file-name', metavar='G',
                    help='name of the output file')
args = parser.parse_args()

dtype = torch.float64
torch.set_default_dtype(dtype)

in_file = open(str(args.data_path) + '0.csv', 'r')
reader = csv.reader(in_file)
reader.next()
skip = True
episodes = []
tot_ep = 0
init_angles = [-0.38999412850956094, -0.575235548734848, 0.002277698221086588, 2.141809310862694, -0.36427095792419095, 0.004169374681665339, 3.1154025545442323]
new_start = False
no_steps = 0
end = 0.0
eop = {32, 74, 126, 178, 211, 248, 282, 323, 371, 408, 443, 475, 518, 552, 583, 626, 666, 691, 765, 801, 851, 892, 929, 957, 995, 1031, 1062, 1093, 1115, 1149, 1191, 1227, 1257, 1284, 1321, 1354, 1383, 1429, 1461, 1500, 1534, 1567, 1606, 1639, 1681, 1703, 1732, 1771, 1796, 1840, 1883} 
print(len(eop))
ep_im, ep_s, ep_a = [], [], []
images, states, actions = [], [], []
hole = np.array([0.423, 0.00, -0.13]) 
disclude = {18}
prints = True
init_angles = []
for e in reader:
    c = int(e[0])
    r0, r1, r2, r3, r4, r5, r6 = float(e[2]), float(e[3]), float(e[4]), float(e[5]), float(e[6]), float(e[7]), float(e[8])
    v0, v1, v2, v3, v4, v5, v6 = float(e[9]), float(e[10]), float(e[11]), float(e[12]), float(e[13]), float(e[14]), float(e[15])
    t0, t1, t2, t3, t4, t5, t6 = float(e[16]), float(e[17]), float(e[18]), float(e[19]), float(e[20]), float(e[21]), float(e[22])
    ex, ey, ez, eox, eoy, eoz, eow = float(e[23]), float(e[24]), float(e[25]), float(e[26]), float(e[27]), float(e[28]), float(e[29])
    evx, evy, evz = float(e[30]), float(e[31]), float(e[32])
    f = float(e[33])
    en = float(e[34])
    im = cv2.imread(str(args.data_path)+str(c)+'.jpeg')
    joint_angles = [r0, r1, r2, r3, r4, r5, r6]
    cos_joint_pos = [math.cos(val) for val in joint_angles]
    sin_joint_pos = [math.sin(val) for val in joint_angles]
    joint_vel = [v0, v1, v2, v3, v4, v5, v6]
    joint_tor = [t0, t1, t2, t3, t4, t5, t6]
    end_pos = [ex, ey, ez]
    end_orient = [eox, eoy, eoz, eow]
    end_vel = [evx, evy, evz]
    force = f
    end = en
    #action = joint_tor
    action = joint_vel
    #action = [r0, r1, r2, r3, r4, r5, r6]
    if c in eop:
        end = 1
    displacement = np.array(hole) - np.array(end_pos)
    distance = float(np.linalg.norm(displacement)) 
    state = end_pos + end_orient + end_vel + list(hole) + list(displacement) + [distance, force, end]
    ep_im.append(im)
    ep_s.append(state)
    ep_a.append(action) 
    if prints:
        iang = {'right_j6': r6, 'right_j5': r5, 'right_j4': r4, 'right_j3': r3, 'right_j2': r2, 'right_j1': r1, 'right_j0': r0}
        init_angles.append(iang)
    prints = False
    if c in eop:
        if tot_ep not in disclude:
            for ii, ss, aa in itertools.izip(ep_im, ep_s, ep_a):
                images.append(ii)
                states.append(ss)
                actions.append(aa)
            print(len(ep_s), len(ep_im), len(ep_a), c, tot_ep)
        tot_ep += 1
        ep_im, ep_s, ep_a = [], [], []
        prints = True

tot_ep = len(images)
print(tot_ep)
images, states, actions = zip( *sorted(zip(images, states, actions), key = lambda x: len(x[0]), reverse = True))

states = torch.from_numpy(np.stack(states)).to(torch.float)
actions = torch.from_numpy(np.stack(actions)).to(torch.float)

pickle.dump(([(states, actions)], None), open(os.path.join(assets_dir(), 'expert_traj/'+ args.out_file_name + '.p'), 'wb'))
pickle.dump(init_angles, open('obj/init_angles.pkl', 'wb'))

'''ep_len = []
for i in range(tot_ep):
    ep_len.append(len(states[i]))

im_fill = np.zeros((128, 128, 3))
s_fill = np.zeros(len(states[0][0])).tolist()
a_fill = np.zeros(len(actions[0][0])).tolist()
batches = []
li = 0
ei = tot_ep 
for i in range(1):
    ind = slice(li, ei)
    bim, bs, ba, blen = images[ind], states[ind], actions[ind], list(ep_len[ind])
    longest = len(bs[0])
    print(longest)
    padded_im = []
    for im in bim:
        im += [im_fill] * (longest - len(im))
        padded_im.append(im)

    padded_s = []
    for s in bs:
        s += [s_fill] * (longest - len(s))
        padded_s.append(s)

    padded_a = []
    for a in ba:
        a += [a_fill] * (longest - len(a))
        padded_a.append(a)
    batches.append((tensor(padded_im, dtype = dtype).transpose(-1, -2).transpose(-2,-3)/255,
                 tensor(padded_s, dtype = dtype), tensor(padded_a, dtype = dtype), tensor(blen, dtype = torch.int)))
    li = ei
    ei = tot_ep

for b in batches:
    t1, t2, t3, t4 = b
    print(t1.size(), t2.size(), t3.size(), t4.size())

pickle.dump((batches, None), open(os.path.join(assets_dir(), 'expert_traj/smooth_rnn_vel.p'), 'wb'))'''
