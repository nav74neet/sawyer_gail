from __future__ import division
import torch
import gc
import math

import os
import sys

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))
#print(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))
#sys.path.insert(0, '/home/nilesh/san/sawyer_ws/src/sawyer-robot-learning/sawyer_demonstration/scripts')
gc.enable()


import argparse
import cv2
import rospy

import intera_interface
import rospkg

from std_msgs.msg import String
from std_msgs.msg import Bool

from gazebo_msgs.srv import (
    SpawnModel,
    DeleteModel,
)
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from intera_core_msgs.msg import EndpointState

from intera_interface import CHECK_VERSION

import time
import datetime
import sys
import math

from copy import deepcopy
import numpy as np
import csv
from sensor_msgs.msg import Image
from gazebo_msgs.msg import ModelStates
import random
from utils import CustomController
from utils import NovintFalcon
from sawyer_demonstration.srv import *
from ros_falcon.msg import falconForces
from gazebo_msgs.msg import ContactsState
from geometry_msgs.msg import Point
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Quaternion
from cv_bridge import CvBridge, CvBridgeError
from std_srvs.srv import Empty

from gazebo_msgs.msg import ModelStates
import scipy.signal as signal

#from sawyer_pykdl import sawyer_kinematics

class Gazebo_Env:
    def __init__(self, env_name, init_angles, rate = 20):
        self.env_name = env_name
        rospy.init_node("sawyer_control_script")
        #kin = sawyer_kinematics('right')
        self.rate = rate
        self._rate = rospy.Rate(self.rate)
        self.pause = rospy.ServiceProxy('/gazebo/pause_physics', Empty)
        self.unpause = rospy.ServiceProxy('/gazebo/unpause_physics', Empty)
        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            self.unpause()
        except (rospy.ServiceException) as e:
            print("rosunpause failed!")
        self._limb = intera_interface.Limb('right')
        self.joints_right = self._limb.joint_names()

        self.hole = [0.423, 0.000, -0.120]
        self.end_poss = [0.0, 0.0, 0.0]
        self._pose, self._point, self._q = self._set_initial_pose_of_eef()
        #tmp = [-0.38999412850956094, -0.575235548734848, 0.002277698221086588, 2.141809310862694, -0.36427095792419095, 0.004169374681665339, 3.1154025545442323]
        #print(np.cos(tmp))
        #self.init_angles = {}
        #print(self.joints_right)
        #for i, j in enumerate(self.joints_right):
        #    self.init_angles[j] = tmp[i]
        self.init_angles = init_angles
        joint_angles = [self._limb.joint_angle(j) for j in self.joints_right]
        end_pos = [self._point.x, self._point.y, self._point.z]
        end_vel = [0.0, 0.0, 0.0]
        self.image = None
        self.state = None

        self.collided = False
        self.contact = 0
        self.bridge = CvBridge()

	    #self.peg_publisher = rospy.Publisher('/peg_position', Point, queue_size=10)
        rospy.Subscriber('/peg_contact_sensor_state', ContactsState, self._peg_contact_sensor_cb)
        rospy.Subscriber('/hole_contact_sensor_state', ContactsState, self._hole_contact_sensor_cb)
        #self.camera_sub = rospy.Subscriber('/front_camera/camera/image_raw', Image, self.handle_image_cb)
        self.update_state()

        self.count_steps = 0
        self.MAX_STEPS = 1000
        self.state_dim = len(self.state[1])
        self.action_dim = len(self.joints_right)
        self.im_dim = 256
        self.im_ch = 3
        self.distance_weight = -3
        self.sine_weight = 0.1
        self.radius = 0.040




    def _set_initial_pose_of_eef(self):
        # End effector orientation is fixed.
        q = Quaternion()
        q.x = 0.642161760993
        q.y = 0.766569045326
        q.z = 0.000271775440016
        q.w = 0.00031241683589

        point = Point()
        pose = Pose()

        pose.position = point
        pose.orientation = q

        return pose, point, q

    def handle_image_cb(self, msg):
        try:
            # Convert your ROS Image message to OpenCV2
            self.image = self.bridge.imgmsg_to_cv2(msg, "bgr8")
        except CvBridgeError, e:
            print(e)
    def _peg_contact_sensor_cb(self, data):
        if data.states:
            self.contact = 1
        else:
            self.contact = 0
    def _hole_contact_sensor_cb(self, data): #callback for the hole's contact sensor
        if data.states:
            self.collided = True
        #else:
        #    self.collided = False

    def update_state(self):
        eef = self._limb.endpoint_pose()
        end_vel = self._limb.endpoint_velocity()['linear']
        joint_angles = [self._limb.joint_angle(j) for j in self.joints_right]
        cos_joint_pos = [math.cos(ja) for ja in joint_angles]
        sin_joint_pos = [math.sin(ja) for ja in joint_angles]
        joint_vel = [self._limb.joint_velocity(j) for j in self.joints_right]
        end_pos = [eef['position'].x, eef['position'].y, eef['position'].z]
        end_orient = [eef['orientation'].x, eef['orientation'].y, eef['orientation'].z, eef['orientation'].w]
        end_vel = [end_vel.x, end_vel.y, end_vel.z]
        self.end_poss = np.array(end_pos)
        disp = (np.array(self.hole) - np.array(end_pos))
        distance = float(np.linalg.norm(disp))
        self.state = (self.image, cos_joint_pos + sin_joint_pos + joint_vel + end_pos + end_orient + end_vel + self.hole + list(disp)
                      + [distance, self.contact, int(self.collided)])


    def check_end(self):                  #end of episode
        if self.collided:
            return True
        else:
            return False
        # return self.state[2][2] <= 0.774


    def reset(self):
        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            self.unpause()
        except (rospy.ServiceException) as e:
            print("rosunpause failed!")

        idx = random.randint(0, len(self.init_angles)-1)
        tmp = {'right_j6': 2.6341108402025846, 'right_j5': -0.09881254041094753, 'right_j4': 0.09323309331150138, 'right_j3': 2.3524184835294513, 'right_j2': 0.012295990854670754, 'right_j1': -0.6839585138100157, 'right_j0': -0.4208275745414778}


        self._limb.move_to_joint_positions(tmp)
        self._limb.move_to_joint_positions(self.init_angles[idx], timeout = 3.0)

        # Update current state
        self.update_state()
        self.count_steps = 0
        self.collided = False
        self.contact = 0

        rospy.wait_for_service('/gazebo/pause_physics')
        try:
            self.pause()
        except (rospy.ServiceException) as e:
            print("rospause failed!")

        return self.state



    def step(self, action):
        reward = 0.0
        '''J = kin.jacobian()
        print(type(J), J.shape)
        x = np.matmul(J, np.array(action))
        print(x.shape)
        x_t = np.array([x[0], x[1], x[2], 0.642, 0.766, 0.0]
        J_inv = np.linalg.pinv(J)
        action = np.matmul(J_inv, x_t)
        print(action.shape)
        asas'''
        joint_angles = {}
        for i, j in enumerate(self.joints_right):
            joint_angles[j] = action[i]

        '''if self._limb.fk_request_in_collision(joint_angles):
            print("Position not reachable.")
        else:
            self._limb.set_joint_positions(joint_angles)'''

        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            self.unpause()
        except (rospy.ServiceException) as e:
            print("rosunpause failed!")
        #for _ in range(10):
        self._limb.set_joint_velocities(joint_angles)
        self._rate.sleep()

        rospy.wait_for_service('/gazebo/pause_physics')
        try:
            self.pause()
        except (rospy.ServiceException) as e:
            print("rospause failed!")

        self.update_state()
        #vector = self.end_poss - self.hole
        #distance = np.linalg.norm(vector)
        #sin_dist = math.pow(vector[2]/(distance + 1e-8), 2)
        #reward = (self.distance_weight * distance + self.sine_weight * sin_dist)
        #reward = (self.state[1][24]*0.6422 + self.state[1][25]*0.7666 + self.state[1][26] * 0.00027 + self.state[1][27] * 0.0003)
        #reward = reward * reward 
        done = self.check_end()
        
        if done:
            reward = 20
        #reward = reward * 0.1 + 0.1 * distance

        self.count_steps += 1

        return self.state, reward, done, None



    def __del__(self):
        self.reset()
        print("Exiting...")
        rospy.wait_for_service('/gazebo/unpause_physics')
        try:
            self.unpause()
        except (rospy.ServiceException) as e:
            print("rosunpause failed!")


'''
env = Gazebo_Env("sawyer", 20)
state = env.reset()
done = False
c = 0
st = time.time()
while not done:
    action = np.random.uniform(size = 7) * 1
    state, r, done, _ = env.step(action)
    im, s = state
    if c > 300:
        break
    c += 1
    print(c)
print((time.time() - st))
stenv.__del__()
#'''
