from __future__ import division
import argparse
#import gym
import os
import sys
import pickle
import time
import math
import warnings
import random
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
#sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '../../..')))
#print(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
from gazebo_env import Gazebo_Env
from util import *
from models.mlp_policy import Policy
from models.mlp_critic import Value
#from models.mlp_policy_disc import DiscretePolicy
from models.mlp_discriminator import Discriminator
from torch import nn
from core.ppo import ppo_step
from core.common import estimate_advantages
from core.agent import Agent

dtype = torch.float
torch.set_default_dtype(dtype)

def save_obj(obj, name ):
    if not os.path.exists('obj/'):
        os.makedirs('obj/')
    with open('obj/'+ name + '.pkl', 'wb') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    #'''
    try:
        with open('obj/' + name + '.pkl', 'rb') as f:
            return pickle.load(f)
    except:
        return -1

class GAIL:
    def __init__(self, args, env, img_ch, img_feat_dim, hid_im_size, state_dim, action_dim, rnn_hid_size, expert_traj, running_state):

        self.device = torch.device('cuda', index=args.gpu_index) if torch.cuda.is_available() else torch.device('cpu')
        self.disc_state_dim = 19
        self.action_dim = action_dim

        if args.models_path is None:
            policy_net = Policy(img_ch, img_feat_dim, hid_im_size, state_dim, action_dim, rnn_hid_size, log_std=args.log_std)
            value_net = Value(img_ch, img_feat_dim, hid_im_size, state_dim, rnn_hid_size)
            discrim_net = Discriminator(img_ch, img_feat_dim, hid_im_size, self.disc_state_dim + action_dim, rnn_hid_size)
        else:
            policy_net, value_net, discrim_net = load_obj(args.models_path)
            print("Models Loaded")
        self.discrim_criterion = nn.BCEWithLogitsLoss()
        to_device(self.device, policy_net, value_net, discrim_net)

        optimizer_policy = torch.optim.Adam(policy_net.parameters(), lr=args.learning_rate)
        optimizer_value = torch.optim.Adam(value_net.parameters(), lr=args.learning_rate)
        optimizer_discrim = torch.optim.Adam(discrim_net.parameters(), lr=args.learning_rate/100)



        self.model = (policy_net, value_net, discrim_net)
        self.optim = (optimizer_policy, optimizer_value, optimizer_discrim)
        self.expert_traj = []
        if args.train:
            for batch in expert_traj:
                t2, t3= batch
                t2 = t2.to(self.device).to(dtype)
                t3 = t3.to(self.device).to(dtype)
                self.expert_traj.append((t2, t3))
        self.args = args

        """create agent"""

    def re_init(self):
        policy_net, value_net, discrim_net = self.model
        optimizer_policy = torch.optim.Adam(policy_net.parameters(), lr=self.args.learning_rate)
        optimizer_value = torch.optim.Adam(value_net.parameters(), lr=self.args.learning_rate)
        optimizer_discrim = torch.optim.Adam(discrim_net.parameters(), lr=self.args.learning_rate/3)
        self.optim = (optimizer_policy, optimizer_value, optimizer_discrim)


    def expert_reward(self, state, action):
        policy_net, value_net, discrim_net = self.model
        discrim_input = (state[:, 21:], action)
        with torch.no_grad():
            logits = discrim_net(discrim_input)
            return -math.log(torch.sigmoid(logits.view(-1)).item() + 1e-8)

    def update_params(self, batch, i_iter):
        # optimization epoch number and batch size for PPO
        optim_epochs = 5
        d_epochs = 2
        optim_batch_size = 500
        recurrence = 50
        self.batch_num = 0
        d_loss = 0.0
        policy_net, value_net, discrim_net = self.model
        optimizer_policy, optimizer_value, optimizer_discrim = self.optim

        # TODO: FIX PADDED LOSS VALUES
        """update discriminator"""
        states = torch.from_numpy(np.stack(batch.state)).to(dtype).to(self.device)
        states = states[:, :, 21:]
        actions = torch.from_numpy(np.stack(batch.action)).to(dtype).to(self.device)
        indices = torch.cat([i * args.episode_len + torch.arange(l) for i, l in enumerate(batch.episode_lens)], dim = 0).to(torch.long).to(self.device)
        states = states.view(-1, self.disc_state_dim)[indices]
        actions = actions.view(-1, self.action_dim)[indices]
        e_states, e_actions = self.expert_traj[0]
        for _ in range(d_epochs):
            if i_iter % self.args.update_d_interval < 5:
                perm = torch.randperm(states.shape[0]).to(self.device)
                i_states, i_actions = states[perm], actions[perm]
                perm = torch.randperm(e_states.shape[0]).to(self.device)
                i_e_states, i_e_actions = e_states[perm], e_actions[perm]

                expert_state_actions = (i_e_states, i_e_actions)
                generator_state_actions = (i_states, i_actions)
                g_o = discrim_net(generator_state_actions)
                e_o = discrim_net(expert_state_actions)
                #g_o, e_o = g_o.view(-1), e_o.view(-1)

                optimizer_discrim.zero_grad()
                discrim_loss = self.discrim_criterion(g_o, ones((g_o.shape[0], 1), device=self.device)) + \
                    self.discrim_criterion(e_o, zeros((e_o.shape[0], 1), device=self.device))
                discrim_loss.backward()
                optimizer_discrim.step()
                d_loss += discrim_loss.item()

        d_loss /= d_epochs

        #'''
        """perform mini-batch PPO update"""
        #images = torch.tensor(np.stack(batch.image), dtype = dtype, device = self.device).transpose(-1, -2).transpose(-2,-3)/255
        states = torch.tensor(np.stack(batch.state), dtype = dtype, device = self.device)
        hiddens = batch.hidden.to(dtype).to(self.device)
        next_hiddens = batch.next_hidden.to(dtype).to(self.device)
        fixed_log_probs = batch.log_prob.to(dtype).to(self.device)
        actions = torch.from_numpy(np.stack(batch.action)).to(dtype).to(self.device)
        rewards = torch.from_numpy(np.stack(batch.reward)).to(dtype).to(self.device)
        masks = torch.from_numpy(np.stack(batch.mask)).to(dtype).to(self.device)
        lengths = torch.tensor(np.stack(batch.episode_lens), dtype = torch.int, device = self.device)
        indices = torch.cat([i * args.episode_len + torch.arange(l) for i, l in enumerate(batch.episode_lens)], dim = 0).to(torch.long).to(self.device)
        e_bs = 100
        optim_iter_num = states.shape[0]//e_bs
        values, values_hidden, hid = [], [], None
        with torch.no_grad():
            hid = torch.zeros(states.size(0), value_net.rnn_hid_size).to(dtype).to(self.device)
            for i in range(args.episode_len):
                values_hidden.append(hid.unsqueeze(1))
                val, hid = value_net((states[:, i], hid))
                values.append(val.unsqueeze(1))
            values = torch.cat(values, dim = 1)
            values_hiddens = torch.cat(values_hidden, dim = 1)
        batch_size = states.shape[0] * args.episode_len
        #images = images.view(batch_size, images.shape[-3], images.shape[-2], images.shape[-1])[indices]
        states = states.view(batch_size, -1)[indices]
        hiddens = hiddens.view(batch_size, -1)[indices]
        actions = actions.view(batch_size, -1)[indices]
        rewards = rewards.view(batch_size, -1)[indices]
        masks = masks.view(batch_size, -1)[indices]
        fixed_log_probs = fixed_log_probs.view(batch_size, -1)[indices]
        values = values.view(batch_size, -1)[indices]
        values_hiddens = values_hiddens.view(batch_size, -1)[indices]

        """get advantage estimation from the trajectories"""
        advantages, returns = estimate_advantages(rewards, masks, values, self.args.gamma, self.args.tau, self.device)
        ppo_data = (states, hiddens, values_hiddens, actions, advantages, returns, fixed_log_probs, masks)

        ppo_step(policy_net, value_net, optimizer_policy, optimizer_value, ppo_data, optim_epochs, optim_batch_size,
                 self.args.episode_len, recurrence, self.args.clip_epsilon, self.args.l2_reg)

        self.model = (policy_net, value_net, discrim_net)
        self.optim = (optimizer_policy, optimizer_value, optimizer_discrim)

        return d_loss

    def train(self, agent):
        for m in self.model:
            m.to(self.device)
            m.train()
        for i_iter in range(self.args.max_iter_num):
            """generate multiple trajectories that reach the minimum batch_size"""
            policy_net, value_net, discrim_net = self.model
            #discrim_net.to(torch.device('cpu'))
            batch, log = agent.collect_samples(self.args.min_batch_size)
            #save_obj((batch,log), 'agent_data')
            #batch, log = load_obj('agent_data')
            #discrim_net.to(self.device)

            t0 = time.time()
            d_loss = self.update_params(batch, i_iter)
            t1 = time.time()

            if i_iter % self.args.log_interval == 0:
                print('{}\tT_sample {:.4f}\tT_update {:.4f}\texpert_R_avg {:.2f}\tR_avg {:.2f}\tDiscrim loss {:.2f}'.format(
                    i_iter, log['sample_time'], t1-t0, log['avg_c_reward'], log['avg_reward'], d_loss))

            if log['sample_time'] > 800:
                print("Error in Simulation... Exiting...")
                sys.exit(0)

            if self.args.save_model_interval > 0 and (i_iter+1) % self.args.save_model_interval == 0 and not math.isnan(log['avg_c_reward']):
                print("Saving models")
                policy_net, value_net, discrim_net = self.model
                to_device(torch.device('cpu'), policy_net, value_net, discrim_net)
                file_num = i_iter // 100
                save_obj((policy_net, value_net, discrim_net), self.args.env_name + '_gail_rl_small_' + str(file_num))
                to_device(self.device, policy_net, value_net, discrim_net)
                self.model = (policy_net, value_net, discrim_net)


            """clean up gpu memory"""
            del batch
            torch.cuda.empty_cache()

        for model in self.model:
            model.to(torch.device('cpu'))




if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='PyTorch GAIL example')
    parser.add_argument('--env-name', default="sawyer", metavar='G',
                        help='name of the environment to run')
    parser.add_argument('--expert-traj-path', metavar='G',
                        help='path of the expert trajectories')
    parser.add_argument('--render', action='store_true', default=False,
                        help='render the environment')
    parser.add_argument('--log-std', type=float, default=-1.0, metavar='G',
                        help='log std for the policy (default: -1.0)')
    parser.add_argument('--gamma', type=float, default=0.99, metavar='G',
                        help='discount factor (default: 0.99)')
    parser.add_argument('--tau', type=float, default=0.95, metavar='G',
                        help='gae (default: 0.95)')
    parser.add_argument('--l2-reg', type=float, default=1e-3, metavar='G',
                        help='l2 regularization regression (default: 1e-3)')
    parser.add_argument('--learning-rate', type=float, default=3e-4, metavar='G',
                        help='learning rate for all models (default: 3e-4)')
    parser.add_argument('--clip-epsilon', type=float, default=0.1, metavar='N',
                        help='clipping epsilon for PPO')
    parser.add_argument('--num-threads', type=int, default=1, metavar='N',
                        help='number of threads for agent (default: 1)')
    parser.add_argument('--min-batch-size', type=int, default=2048, metavar='N',
                        help='minimal batch size per PPO update (default: 2048)')
    parser.add_argument('--max-iter-num', type=int, default=500, metavar='N',
                        help='maximal number of main iterations (default: 500)')
    parser.add_argument('--log-interval', type=int, default=1, metavar='N',
                        help='interval between training status logs (default: 10)')
    parser.add_argument('--save-model-interval', type=int, default=100, metavar='N',
                        help="interval between saving model (default: 0, means don't save)")
    parser.add_argument('--gpu-index', type=int, default=0, metavar='N')

    parser.add_argument('--train', action = 'store_true')
    parser.add_argument('--demo', action = 'store_true')
    parser.add_argument('--episode-len', type=int, default=400, metavar='N',
                        help='number of timesteps in an episode (default: 400)')
    parser.add_argument('--sim-fps', type=int, default=200, metavar='N',
                        help='rate of simulation (default: 200)')
    parser.add_argument('--model-path', default=None, metavar='G',
                        help='path of the complete gail object')
    parser.add_argument('--models-path', default=None, metavar='G',
                        help='path of separate models')
    parser.add_argument('--update-d-interval', type = int, default=10, metavar='G',
                        help='Interations per discriminator updates')
    args = parser.parse_args()

    if torch.cuda.is_available():
        torch.cuda.set_device(args.gpu_index)

    '''
        env: Class object
            Methods:
                reset():    Resets the environment and returns the initial state
                step(action):   Performs the action in environment and returns:
                                    next_state, reward(0 for gail), episode-done-boolean, something
                render():   (Optional) Enable GUI (by default no GUI)
    '''
    print("GAIL...")
    iang = load_obj("init_angles")
    env = Gazebo_Env("sawyer", iang, args.sim_fps)
    state_dim = env.state_dim
    action_dim = env.action_dim
    im_dim = env.im_dim
    im_ch = env.im_ch
    rnn_hid_size = 128
    print(state_dim, action_dim, im_dim, im_ch)
    if args.train:
        expert_traj, running_state = pickle.load(open(args.expert_traj_path, "rb"))
    else:
        expert_traj, running_state = None, None

    if args.model_path is not None:
        print("Loading ", args.model_path)
        with open(args.model_path, 'rb') as f:
            gail = pickle.load(f)
        gail.args = args
        #gail.re_init()
    else:
        gail = GAIL(args, env, im_ch, 128, 6272, state_dim, action_dim, rnn_hid_size, expert_traj, running_state)

    agent = Agent(env, gail.model[0], gail.device, custom_reward=gail.expert_reward, episode_len = args.episode_len,
                        running_state=running_state, render=args.render, num_threads=args.num_threads, update_rs = False)
    if args.train:
        print("Training...")
        try:
            gail.train(agent)
            print("Saving...")
            pickle.dump(gail, open(os.path.join(assets_dir(), 'learned_models/{}_gail_final_rl_small.p'.format(args.env_name)), 'wb'))
        except KeyboardInterrupt:
            print("Exiting")
    elif args.demo:
        print("Demo")
        for model in gail.model:
            model.train()
        agent.demo()
    env.__del__()
