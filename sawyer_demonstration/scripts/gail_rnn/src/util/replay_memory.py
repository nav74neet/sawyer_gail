from __future__ import division 
from collections import namedtuple
import random
import numpy as np
import torch

# Taken from
# https://github.com/pytorch/tutorials/blob/master/Reinforcement%20(Q-)Learning%20with%20PyTorch.ipynb

Transition = namedtuple('Transition', ('image', 'state', 'hidden', 'action', 'mask', 'next_image', 'next_state',
                                       'next_hidden', 'reward'))


class Memory(object):
    def __init__(self):
        self.memory = []

    def push(self, *args):
        """Saves a transition."""
        self.memory.append(Transition(*args))

    def sample(self, batch_size=None):
        if batch_size is None:
            return Transition(*zip(*self.memory))
        else:
            random_batch = random.sample(self.memory, batch_size)
            return Transition(*zip(*random_batch))

    def append(self, new_memory):
        self.memory += new_memory.memory

    def __len__(self):
        return len(self.memory)

class Episodes(object):
    def __init__(self, max_episode_len, img_size, state_dim, action_dim):
        self.image = []
        self.state = []
        self.hidden = torch.tensor([])
        self.action = []
        self.log_prob = torch.tensor([])
        self.mask = []
        self.next_state = []
        self.next_hidden = torch.tensor([]) 
        self.next_image = []
        self.reward = []
        
        self.ep_img = []
        self.ep_states = []
        self.ep_hidden = []
        self.ep_actions = []
        self.ep_log_prob = []
        self.ep_rewards = []
        self.ep_masks = []
        self.ep_next_states = []
        self.ep_next_img = []
        self.ep_next_hidden = []

        self.episode_lens = []
        self.max_episode_len = max_episode_len

        self.img_fill = np.zeros((img_size, img_size, 3))
        self.state_fill = np.zeros(state_dim).tolist()
        self.action_fill = np.zeros(action_dim).tolist()
        self.log_prob_fill = torch.zeros(1, 1, dtype = torch.float)

    def push(self, im, s, h, a, lp, m , nim, ns, nh, r):
        #self.ep_img.append(im)
        self.ep_states.append(s)
        self.ep_hidden.append(h)
        self.ep_actions.append(a)
        self.ep_log_prob.append(lp)
        self.ep_rewards.append(r)
        self.ep_masks.append(m)
        self.ep_next_states.append(ns)
        #self.ep_next_img.append(nim)
        self.ep_next_hidden.append(nh)
        

    def end_episode(self):
        padding_len = self.max_episode_len - len(self.ep_states)
        #self.image.append(self.ep_img + [self.img_fill] * padding_len)
        self.state.append(self.ep_states + [self.state_fill] * padding_len)
        self.action.append(self.ep_actions + [self.action_fill] * padding_len)
        self.reward.append(self.ep_rewards + [0.0] * padding_len)
        self.mask.append(self.ep_masks + [0] * padding_len)
        
        
        ep_log_prob = torch.cat(self.ep_log_prob + [self.log_prob_fill] * padding_len, dim = 1)
        self.log_prob = torch.cat([self.log_prob, ep_log_prob], dim = 0)
        ep_hidden = torch.cat(self.ep_hidden + [torch.zeros_like(self.ep_hidden[0])] * padding_len, dim = 0)
        self.hidden = torch.cat([self.hidden, ep_hidden.unsqueeze(0)], dim = 0)
        ep_next_hidden = torch.cat(self.ep_next_hidden + [torch.zeros_like(self.ep_next_hidden[0])] * padding_len, dim = 0)
        self.next_hidden = torch.cat([self.next_hidden, ep_next_hidden.unsqueeze(0)], dim = 0)
        
        self.next_state.append(self.ep_next_states)
        #self.next_image.append(self.ep_next_img)
        
        self.episode_lens.append(len(self.ep_states))
        
        self.ep_img = []
        self.ep_states = []
        self.ep_hidden = []
        self.ep_actions = []
        self.ep_log_prob = []
        self.ep_rewards = []
        self.ep_masks = []
        self.ep_next_states = []
        self.ep_next_img = []
        self.ep_next_hidden = []

    def append(self, new_episodes):
        #self.image += new_episodes.image
        self.state += new_episodes.state
        self.hidden = torch.cat([self.hidden, new_episodes.hidden], dim = 0) 
        self.action += new_episodes.action
        self.log_prob = torch.cat([self.log_prob, new_episodes.log_prob], dim = 0) 
        self.mask += new_episodes.mask
        self.next_state += new_episodes.next_state
        self.next_hidden = torch.cat([self.next_hidden, new_episodes.next_hidden], dim = 0)
        #self.next_image += new_episodes.next_image
        self.reward += new_episodes.reward
        self.episode_lens += new_episodes.episode_lens

    def sample(self):
        return self

    def __len__(self):
        return np.sum(self.episode_lens)
