from __future__ import division
import multiprocessing
from util.replay_memory import *
from util.pytorch import *
import math
import time


def collect_samples(pid, queue, env, policy, custom_reward, device,
                    mean_action, render, running_state, min_batch_size, update_rs,
                    episode_len = 500):
    torch.randn(pid)
    log = dict()
    #memory = Memory()
    num_steps = 0
    total_reward = 0
    min_reward = 1e6
    max_reward = -1e6
    total_c_reward = 0
    min_c_reward = 1e6
    max_c_reward = -1e6
    num_episodes = 0

    episodes = Episodes(episode_len, policy.image_size, policy.state_dim, policy.action_dim)
    s1 = time.time()

    while num_steps < min_batch_size:
        state = env.reset()
        hidden = torch.zeros(1, policy.rnn_hid_size, device = device)
        reward_hidden = None
        if running_state is not None:
            state = running_state(state, update_rs)
        reward_episode = 0
        tm = 0
        for t in range(episode_len):
            state_var = policy.get_state_var(state, device = device)
            with torch.no_grad():
                if mean_action:
                    action_t, t1, t2, hidden = policy((state_var, hidden))
                else:
                    action_t, log_prob, next_hidden = policy.select_action((state_var, hidden))
            action = action_t[0].cpu().numpy().astype(np.float64)
            next_state, reward, done, _ = env.step(action)
            reward_episode += reward
            if running_state is not None:
                next_state = running_state(next_state, update_rs)

            if custom_reward is not None:
                reward = custom_reward(state_var, action_t) + reward
                total_c_reward += reward
                min_c_reward = min(min_c_reward, reward)
                max_c_reward = max(max_c_reward, reward)

            mask = 0 if (done or t == episode_len - 1) else 1
            if hidden is None:
                hidden = torch.tensor([])
            episodes.push(state[0], state[1], hidden.cpu(), action, log_prob.cpu(), mask, next_state[0], next_state[1], next_hidden.cpu(), reward)

            if render:
                env.render()
            if done:
                break

            state = next_state
            hidden = next_hidden
            del state_var
        episodes.end_episode()
        # log stats
        num_steps += (t + 1)
        num_episodes += 1
        total_reward += reward_episode
        min_reward = min(min_reward, reward_episode)
        max_reward = max(max_reward, reward_episode)

    log['num_steps'] = num_steps
    log['num_episodes'] = num_episodes
    log['total_reward'] = total_reward
    log['avg_reward'] = total_reward / num_episodes
    log['max_reward'] = max_reward
    log['min_reward'] = min_reward
    if custom_reward is not None:
        log['total_c_reward'] = total_c_reward
        log['avg_c_reward'] = total_c_reward / num_steps
        log['max_c_reward'] = max_c_reward
        log['min_c_reward'] = min_c_reward


    if queue is not None:
        queue.put([pid, episodes, log])
    else:
        return episodes, log


def merge_log(log_list):
    log = dict()
    log['total_reward'] = sum([x['total_reward'] for x in log_list])
    log['num_episodes'] = sum([x['num_episodes'] for x in log_list])
    log['num_steps'] = sum([x['num_steps'] for x in log_list])
    log['avg_reward'] = log['total_reward'] / log['num_episodes']
    log['max_reward'] = max([x['max_reward'] for x in log_list])
    log['min_reward'] = min([x['min_reward'] for x in log_list])
    if 'total_c_reward' in log_list[0]:
        log['total_c_reward'] = sum([x['total_c_reward'] for x in log_list])
        log['avg_c_reward'] = log['total_c_reward'] / log['num_steps']
        log['max_c_reward'] = max([x['max_c_reward'] for x in log_list])
        log['min_c_reward'] = min([x['min_c_reward'] for x in log_list])

    return log


class Agent:

    def __init__(self, env, policy, device, custom_reward = None, episode_len = 500,
                 mean_action=False, render=False, running_state=None, num_threads=1, update_rs = True):
        self.env = env
        self.policy = policy
        self.device = device
        self.custom_reward = custom_reward
        self.mean_action = mean_action
        self.running_state = running_state
        self.update_rs = update_rs
        self.render = render
        self.num_threads = num_threads
        self.episode_len = episode_len

    def collect_samples(self, min_batch_size):
        t_start = time.time()
        #to_device(torch.device('cpu'), self.policy)
        thread_batch_size = int(math.floor(min_batch_size / self.num_threads))
        queue = multiprocessing.Queue()
        workers = []

        for i in range(self.num_threads-1):
            worker_args = (i+1, queue, self.env, self.policy, self.custom_reward, self.device, self.mean_action,
                           False, self.running_state, thread_batch_size, self.update_rs, self.episode_len)
            workers.append(multiprocessing.Process(target=collect_samples, args=worker_args))
        for worker in workers:
            worker.start()

        memory, log = collect_samples(0, None, self.env, self.policy, self.custom_reward, self.device,
                                     self.mean_action, self.render, self.running_state, thread_batch_size,
                                     self.update_rs, self.episode_len)

        worker_logs = [None] * len(workers)
        worker_memories = [None] * len(workers)
        for _ in workers:
            pid, worker_memory, worker_log = queue.get()
            worker_memories[pid - 1] = worker_memory
            worker_logs[pid - 1] = worker_log
        for worker_memory in worker_memories:
            memory.append(worker_memory)
        batch = memory.sample()
        if self.num_threads > 1:
            log_list = [log] + worker_logs
            log = merge_log(log_list)
        to_device(self.device, self.policy)
        t_end = time.time()
        log['sample_time'] = t_end - t_start
        log['action_mean'] = np.mean(np.vstack(batch.action), axis=0)
        log['action_min'] = np.min(np.vstack(batch.action), axis=0)
        log['action_max'] = np.max(np.vstack(batch.action), axis=0)
        return batch, log

    def demo(self, max_expert_state_num = 100):
        num_steps = 0
        mean_action = True
        print("Rewards")
        for _ in range(max_expert_state_num):
            reward_episode = 0

            state = self.env.reset()
            hidden = torch.zeros(1, self.policy.rnn_hid_size, device = self.device)
            if self.running_state is not None:
                state = running_state(state, update_rs)
            for t in range(self.episode_len):
                state_var = self.policy.get_state_var(state, device = self.device)
                with torch.no_grad():
                    if mean_action:
                        action_t, t1, t2, hidden = self.policy((state_var, hidden))
                    else:
                        action_t, log_prob, next_hidden = self.policy.select_action((state_var, hidden))
                action = action_t[0].cpu().numpy().astype(np.float64)
                next_state, reward, done, _ = self.env.step(action)
                if self.running_state is not None:
                    next_state = running_state(next_state, update_rs)

                reward_episode += reward
                if self.render:
                    self.env.render()
                if done:
                    break

                if done or num_steps > max_expert_state_num:
                    break

                state = next_state

            print('{:.2f}'.format(reward_episode))
