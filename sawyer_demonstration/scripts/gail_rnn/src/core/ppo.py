from __future__ import division 
import torch
import numpy
import time

def _get_batches_starting_indexes(num_frames, recurrence, num_frames_per_proc, batch_size, batch_num):
    """Gives, for each batch, the indexes of the observations given to
    the model and the experiences used to compute the loss at first.
    First, the indexes are the integers from 0 to `self.num_frames` with a step of
    `self.recurrence`, shifted by `self.recurrence//2` one time in two for having
    more diverse batches. Then, the indexes are splited into the different batches.
    Returns
    -------
    batches_starting_indexes : list of list of int
        the indexes of the experiences to be used at first for each batch
    """

    indexes = numpy.arange(0, num_frames, recurrence)
    indexes = numpy.random.permutation(indexes)

    # Shift starting indexes by self.recurrence//2 half the time
    if batch_num % 2 == 1:
        indexes = indexes[(indexes + recurrence) % num_frames_per_proc != 0]
        indexes += recurrence // 2
        indexes = indexes[indexes < num_frames]

    num_indexes = batch_size // recurrence
    batches_starting_indexes = [indexes[i:i+num_indexes] for i in range(0, len(indexes), num_indexes)]

    return batches_starting_indexes


def ppo_step(policy_net, value_net, optimizer_policy, optimizer_value, ppo_data, epochs, batch_size, episode_len, recurrence, clip_epsilon, l2_reg):

    states, hiddens, values_hiddens, actions, advantages, returns, fixed_log_probs, masks = ppo_data
    num_frames = states.shape[0]
    batch_num = 0
    
    for _ in range(epochs):
        f = True
        for ind in _get_batches_starting_indexes(num_frames, recurrence, episode_len, batch_size, batch_num):
            batch_num += 1
            hid = hiddens[ind]
            val_hid = values_hiddens[ind]
            batch_value_loss = 0
            batch_policy_loss = 0
            for i in range(recurrence):
                inds = (ind + i) % num_frames
                s, a, adv, ret, f_log_probs, m = states[inds], actions[inds], advantages[inds], returns[inds], fixed_log_probs[inds], masks[inds-1]
                """update critic"""
                values_pred, val_hid = value_net((s, val_hid * m))
                value_loss = (values_pred - ret).pow(2).mean()
                # weight decay
                for param in value_net.parameters():
                    value_loss += param.pow(2).sum() * l2_reg
                batch_value_loss += value_loss

                """update policy"""
                log_probs, hid = policy_net.get_log_prob((s, hid * m), a)
                ratio = torch.exp(log_probs - f_log_probs)
                surr1 = ratio * adv
                surr2 = torch.clamp(ratio, 1.0 - clip_epsilon, 1.0 + clip_epsilon) * adv
                policy_surr = -torch.min(surr1, surr2).mean()
                batch_policy_loss += policy_surr
                if i < recurrence - 1:
                    inds = torch.tensor(inds, device = hiddens.device)
                    update_inds =  inds < num_frames - 1
                    inds = inds[update_inds]
                    hiddens[inds + 1] = hid[update_inds].detach()
                    values_hiddens[inds + 1] = val_hid[update_inds].detach()

            batch_value_loss /= recurrence
            batch_policy_loss /= recurrence    

            optimizer_value.zero_grad()
            batch_value_loss.backward()
            optimizer_value.step()

            optimizer_policy.zero_grad()
            batch_policy_loss.backward()
            torch.nn.utils.clip_grad_norm_(policy_net.parameters(), 40)
            optimizer_policy.step()
