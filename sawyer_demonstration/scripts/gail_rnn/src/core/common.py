from __future__ import division 
import torch
from util import to_device

#'''
def estimate_advantages(rewards, masks, values, gamma, tau, device):
    #rewards, masks, values = to_device(torch.device('cpu'), rewards, masks, values)
    tensor_type = type(rewards)
    deltas = tensor_type(rewards.size(0), 1).to(device)
    advantages = tensor_type(rewards.size(0), 1).to(device)

    prev_value = 0
    prev_advantage = 0
    for i in reversed(range(rewards.size(0))):
        deltas[i] = rewards[i] + gamma * prev_value * masks[i] - values[i]
        advantages[i] = deltas[i] + gamma * tau * prev_advantage * masks[i]

        prev_value = values[i, 0]
        prev_advantage = advantages[i, 0]

    '''for i in range(rewards.size(0)):
        if advantages[i] == rewards[i] - values[i]:
            print(i, ".", masks[i])'''

    returns = values + advantages
    advantages = (advantages - advantages.mean()) / advantages.std()

    #advantages, returns = to_device(device, advantages, returns)
    return advantages, returns
