from __future__ import division 
import torch.nn as nn
import torch
from cnn_module import ImageMap
from util.math import *

class GAIL(nn.Module):
    def __init__(self, cnn_module, policy, value, discriminator):
        super(GAIL, self).__init__()
        self.cnn = cnn_module
        self.policy = policy
        self.value = value
        self.discriminator
