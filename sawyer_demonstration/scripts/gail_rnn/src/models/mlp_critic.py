from __future__ import division 
import torch.nn as nn
import torch
from cnn_module import ImageMap

class Value(nn.Module):
    def __init__(self, img_ch, img_feat_dim, hid_im_size, state_dim, rnn_hid_size, hidden_size=(128, 128), activation='tanh'):
        super(Value, self).__init__()
        self.rnn_hid_size = rnn_hid_size

        if activation == 'tanh':
            self.activation = torch.tanh
        elif activation == 'relu':
            self.activation = torch.relu
        elif activation == 'sigmoid':
            self.activation = torch.sigmoid
    
        #self.im_map = ImageMap(img_ch, img_feat_dim, hid_im_size, [8, 4], [4, 2], [16, 32])
        
        last_dim = state_dim
        self.affine_layers = nn.ModuleList()
        for nh in hidden_size:
            self.affine_layers.append(nn.Linear(last_dim, nh))
            last_dim = nh
        
        self.rnn = nn.GRUCell(last_dim, rnn_hid_size)
        #self.rnn = nn.GRU(img_feat_dim + last_dim, rnn_hid_size, 1, batch_first = True)

        self.value_head = nn.Linear(last_dim, 1)
        self.value_head.weight.data.mul_(0.1)
        self.value_head.bias.data.mul_(0.0)

    '''
    def forward(self, x, seq = True):
        (img, s), hidden = x
        img = self.im_map(img, seq)

        for affine in self.affine_layers:
            s = self.activation(affine(s))
            
        x = torch.cat([img, s], dim = -1)
        x, hidden = self.rnn(x, hidden)
        value = self.value_head(x)
        return value, hidden

    '''

    def forward(self, x):
        s, hidden = x
        #img = self.im_map(img)

        for affine in self.affine_layers:
            s = self.activation(affine(s))

        x = s
        #x = torch.cat([img, s], dim = -1)
        hidden = self.rnn(x, hidden)
        value = self.value_head(hidden)
        '''value = []
        for i in range(x.size(1)):
            hidden = self.rnn(x[:, i], hidden)
            value.append(self.value_head(hidden).unsqueeze(1))
        value = torch.cat(value, dim = 1)'''
        return value, hidden

    #'''
        
'''       
m = Value(3, 128, 512, 7)
a = torch.randn(1, 3, 256, 256)
x = torch.randn(1, 7)
x = (a, x)
feat = m(x)
print(feat.size())
print(feat)
#'''
