from __future__ import division 
import torch.nn as nn
import torch
import itertools

class ImageMap(nn.Module):
    def __init__(self, in_channels, out_size, hidden_size, filter_sizes = [4, 16, 4], strides = [1, 8, 2], num_filters = [32, 64, 128], activation='leaky_relu'):
        '''
            input-size: 128 x 128
        '''
        super(ImageMap, self).__init__()
        if activation == 'tanh':
            self.activation = torch.tanh
        elif activation == 'relu':
            self.activation = torch.relu
        elif activation == 'leaky_relu':
            self.activation = torch.nn.functional.leaky_relu
        elif activation == 'sigmoid':
            self.activation = torch.sigmoid
            
        self.cnn_layers = nn.ModuleList()
        self.batchnorm_layers = nn.ModuleList()
        in_channels = in_channels
        for i in range(len(filter_sizes)):
            self.cnn_layers.append(nn.Conv2d(in_channels, num_filters[i], filter_sizes[i], stride = strides[i]))
            self.batchnorm_layers.append(nn.BatchNorm2d(num_filters[i]))
            in_channels = num_filters[i]

        self.linear = nn.Linear(hidden_size, out_size)
        self.linear.weight.data.mul_(0.1)
        self.linear.bias.data.mul_(0.0)

    def forward(self, x, seq = False):
        batch_size = x.size(0)
        if seq:
            seq_size = x.size(1)
        x = x.view(-1, x.size(-3), x.size(-2), x.size(-1))
        for bnorm, cnn in itertools.izip(self.batchnorm_layers, self.cnn_layers):
            x = self.activation(bnorm(cnn(x)))
        if seq:
            x = x.view(batch_size, seq_size, -1)
        else:
            x = x.view(batch_size, -1)
        feat = self.linear(x)
        return feat

''' 
m = ImageMap(3, 128, 1152, [8, 4], [4,2], [16, 32])
a = torch.randn(1, 3, 64, 64)
feat = m(a)
print(feat.size())
'''

