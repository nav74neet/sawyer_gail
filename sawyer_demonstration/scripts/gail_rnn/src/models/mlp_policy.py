from __future__ import division 
import torch.nn as nn
import torch
from cnn_module import ImageMap
from util.math import *
from torch.distributions.normal import Normal

class Policy(nn.Module):
    def __init__(self, img_ch, img_feat_dim, hid_im_size, state_dim, action_dim, rnn_hid_size, hidden_size=(128, 128), activation='tanh', log_std=0):
        super(Policy, self).__init__()
        self.image_size = 128
        self.state_dim = state_dim
        self.action_dim = action_dim
        self.rnn_hid_size = rnn_hid_size

        self.is_disc_action = False
        if activation == 'tanh':
            self.activation = torch.tanh
        elif activation == 'relu':
            self.activation = torch.relu
        elif activation == 'sigmoid':
            self.activation = torch.sigmoid

        self.im_ch = img_ch
        #self.im_map = ImageMap(img_ch, img_feat_dim, hid_im_size, [8, 4], [4,2], [16, 32])
        
        last_dim = state_dim
        self.affine_layers = nn.ModuleList()
        for nh in hidden_size:
            self.affine_layers.append(nn.Linear(last_dim, nh))
            last_dim = nh
        
        #self.rnn = nn.GRU(img_feat_dim + last_dim, rnn_hid_size, 1, batch_first = True)
        self.rnn = nn.GRUCell(last_dim, rnn_hid_size)

        self.action_mean = nn.Linear(last_dim, action_dim)
        self.action_mean.weight.data.mul_(0.1)
        self.action_mean.bias.data.mul_(0.0)

        self.action_log_std = nn.Parameter(torch.ones(1, action_dim) * log_std)

    def forward(self, x):
        '''
            Input:
                x: (image, state), hidden
                    image: 4-D Tensor [batch, channels, width, height]
                    state: 2-D Tensor [batch, state_dim]
                    hidden: 2-D Tensor [batch, rnn_hid_size] or None
            Output:
                action_mean: 2-D Tensor [batch, action_dim]
                action_log_std: ""
                action_std:     ""
                hidden: 2-D Tensor [batch, rnn_hid_size]
        '''
        s, hidden = x
        #img = self.im_map(img)
        for affine in self.affine_layers:
            s = self.activation(affine(s))
        #x = torch.cat([img, s], dim = -1)
        x = s
        ''' 
        if not seq:
            x = x.unsqueeze(1)
            if hidden is not None:
                hidden = hidden.unsqueeze(0)
        '''

        #x, hidden = self.rnn(x, hidden)
        hidden = self.rnn(x, hidden)

        action_mean = self.action_mean(hidden)
        action_log_std = self.action_log_std.expand_as(action_mean)
        action_std = torch.exp(action_log_std)
        #dist = Normal(action_mean, action_std)
        
        return action_mean, action_log_std, action_std, hidden

    def select_action(self, x, mean_action = False):
        action_mean, action_log_std, action_std, hidden = self.forward(x)
        action = torch.normal(action_mean, action_std)
        return action, normal_log_density(action, action_mean, action_log_std, action_std), hidden

    def get_log_prob(self, x, actions):
        action_mean, action_log_std, action_std, hidden = self.forward(x)
        return normal_log_density(actions, action_mean, action_log_std, action_std), hidden
        #dist, hidden = self.forward(x)
        #return dist.log_prob(actions), hidden

    def get_kl(self, x):
        mean1, log_std1, std1, hidden = self.forward(x)

        mean0 = mean1.detach()
        log_std0 = log_std1.detach()
        std0 = std1.detach()
        kl = log_std1 - log_std0 + (std0.pow(2) + (mean0 - mean1).pow(2)) / (2.0 * std1.pow(2)) - 0.5
        return kl.sum(1, keepdim=True)

    def get_fim(self, x):
        mean, _, _, _ = self.forward(x)
        cov_inv = self.action_log_std.exp().pow(-2).squeeze(0).repeat(x.size(0))
        param_count = 0
        std_index = 0
        id = 0
        for name, param in self.named_parameters():
            if name == "action_log_std":
                std_id = id
                std_index = param_count
            param_count += param.view(-1).shape[0]
            id += 1
        return cov_inv.detach(), mean, {'std_id': std_id, 'std_index': std_index}
        
    def get_state_var(self, state, batch_size = 1, device = None):
        im, s = state
        s = torch.tensor(s, device = device).view(batch_size, -1)
        #im = torch.tensor(im, dtype = torch.float, device = device).transpose(1,2).transpose(0,1) / 255
        #print(im.size())
        #if batch_size == 1:
        #    im = im.unsqueeze(0) 
        #im = im.unsqueeze(1)
        #s =  s.unsqueeze(1)
        #im = (torch.tensor(im, dtype = torch.float, device = device).transpose(1,2).transpose(0,1) / 255).view(batch_size, self.im_ch, self.im_sz, self.im_sz) 
        return s
        
        

'''256       
m = Policy(3, 256, 512, 13, 7)
a = torch.randn(1, 3, 256, 256)
x = torch.randn(1, 13)
x = (a, x)
a ,b, c = m(x)
print(a.size())
print(a)
#'''
