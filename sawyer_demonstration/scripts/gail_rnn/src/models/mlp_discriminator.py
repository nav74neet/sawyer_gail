from __future__ import division 
import torch.nn as nn
import torch
from cnn_module import ImageMap


class Discriminator(nn.Module):
    def __init__(self, img_ch, img_feat_dim, hid_im_size, num_inputs, rnn_hid_size, hidden_size=(128, 64), activation='tanh'):
        super(Discriminator, self).__init__()
        if activation == 'tanh':
            self.activation = torch.tanh
        elif activation == 'relu':
            self.activation = torch.relu
        elif activation == 'sigmoid':
            self.activation = torch.sigmoid

        #self.im_map = ImageMap(img_ch, img_feat_dim, hid_im_size, [8, 4], [4, 2], [16, 32])

        self.affine_layers = nn.ModuleList()
        last_dim = num_inputs
        for nh in hidden_size:
            self.affine_layers.append(nn.Linear(last_dim, nh))
            last_dim = nh

        #self.rnn = nn.GRU(last_dim, rnn_hid_size, 1, batch_first = False)

        self.logic = nn.Linear(last_dim, 1)
        self.logic.weight.data.mul_(0.1)
        self.logic.bias.data.mul_(0.0)

    def forward(self, x):        
        s, a = x
        #img = self.im_map(img, seq)

        x = torch.cat([s, a], dim = -1)
        for affine in self.affine_layers:
            x = self.activation(affine(x))
            
        logits = self.logic(x)
        '''if seq:
            print(logits.size())'''
        #prob = torch.sigmoid(self.logic(x))
        return logits
'''       
m = Discriminator(3, 128, 512, 7)
a = torch.randn(1, 3, 256, 256)
x = torch.randn(1, 7)
x = (a, x)
feat = m(x)
print(feat.size())
print(feat)
#'''
