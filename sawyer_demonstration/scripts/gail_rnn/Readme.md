# Generative Adversarial Imitation Learning
  
    Based on work of [Jonathan Ho et al 2016](https://arxiv.org/abs/1606.03476).
    Currently designed to work with Sawyer Robot in Gazebo ROS Kinetic,
    a 7-DOF arm robot to the task of peg in the hole.
    
## Environment Requirements:
      
    * Linux based on Ubuntu 16.04
    * Python 2.7
    * Pytorch 1.0
    * CUDA 9 [Optional; only if want accelerated training using GPU]
    * Gazebo 7 (ROS Kinetic)
    * Sawyer SDK(RethinkRobotics)
        
## Run:
```      
    usage: gail.py [-h] [--env-name G] [--expert-traj-path G] [--render]
                   [--log-std G] [--gamma G] [--tau G] [--l2-reg G]
                   [--learning-rate G] [--clip-epsilon N] [--num-threads N]
                   [--min-batch-size N] [--max-iter-num N] [--log-interval N]
                   [--save-model-interval N] [--gpu-index N] [--train] [--demo]
                   [--episode-len N] [--sim-fps N] [--model-path G]
                   [--models-path G] [--update-d-interval G]

        arguments:
          -h, --help            show this help message and exit
          --env-name G          name of the environment to run
          --expert-traj-path G  path of the expert trajectories
          --render              render the environment
          --log-std G           log std for the policy (default: -1.0)
          --gamma G             discount factor (default: 0.99)
          --tau G               gae (default: 0.95)
          --l2-reg G            l2 regularization regression (default: 1e-3)
          --learning-rate G     learning rate for all models (default: 3e-4)
          --clip-epsilon N      clipping epsilon for PPO
          --num-threads N       number of threads for agent (default: 1)
          --min-batch-size N    minimal batch size per PPO update (default: 2048)
          --max-iter-num N      maximal number of main iterations (default: 500)
          --log-interval N      interval between training status logs (default: 10)
          --save-model-interval N
                                interval between saving model (default: 0, means do not save)
          --gpu-index N
          --train
          --demo
          --episode-len N       number of timesteps in an episode (default: 400)
          --sim-fps N           rate of simulation (default: 200)
          --model-path G        path of the complete gail object
          --models-path G       path of separate models
          --update-d-interval G
                                Interations per discriminator updates
```    
    
### PyTorch GAIL example: Train
* First process expert trajectories using following file:
  
```console
        python gail/save_expert_traj.py --data-path .[PATH TO EXPERT TRAJECTORY DIRECTORY] --out-file-name [EXPERT_TRAJ_OUT_NAME]
```
            Directory should contain a file named "0.csv", containing data in following format:
                Column 0: Row Id
                Column 9-15: Joint velocities
                Column 23-29: End Effector Pose(position and orientation)
                Column 30-32: Enf Effector Velocity(Linear)
                Column 33: Peg Contact Data(0/1)
                Column 34: Hole Contact Data(0/1)
            [EXPERT_TRAJ_OUT_NAME] will be used while running GAIL code.
            
              
* Now Run the GAIL code to start training.
  
```console
        python gail/gail.py --expert-traj-path assets/expert_traj/[EXPERT_TRAJ_OUT_NAME] --save-model-interval 10 --update-d-interval 10 --min-batch-size 1500 --max-iter-num 2000 --sim-fps 30 --episode-len 150 --train
```
### PyTorch GAIL example: Demo
```console
    python gail/gail.py --sim-fps 30 --episode-len 150 --model-name assets/learned_models/[GAIL_OBJ_NAME] --demo
```
        By default GAIL_OBJ_NAME is 'sawyer_gail_final_rl_small.p'
        
        
### Reference:
* https://github.com/Khrylx/PyTorch-RL
* https://github.com/lcswillems/torch-rl
    