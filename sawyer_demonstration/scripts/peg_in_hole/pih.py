#!/usr/bin/env python
# peg in hole task script
import rospy
from demonstrations import Demonstration
import random
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)

from gazebo_msgs.srv import (
    SpawnModel,
    DeleteModel,
)
import rospkg
import time
from sawyer_demonstration.srv import StartRecording
from sawyer_demonstration.srv import StopRecording
######################################
from gazebo_msgs.msg import ModelState
from gazebo_msgs.msg import LinkStates
from geometry_msgs.msg import Twist
######################################

class PegInHole(object):
    def __init__(self, controller):
        self.rate = rospy.Rate(10)
        self.demo = Demonstration("pih")
        self.controller = controller
        self.record = False

        #######################
        self.robot_hand_z = 0.0
        self.set_peg_ft_pose_pub = rospy.Publisher('/gazebo/set_model_state', ModelState, queue_size=10)
        self.gazebo_links_states = rospy.Subscriber('/gazebo/link_states', LinkStates, self.gazebo_links_cb)
        #######################

    def gazebo_links_cb(self, data):
        self.robot_hand_z = data.pose[9].position.z


    # def spawn_hole(self):
    #     hole_pose=Pose(position=Point(x=0.60, y=0.0, z=0.630))
    #     # Get Models' Path
    #     model_path = rospkg.RosPack().get_path('sawyer_gazebo_env')+"/models/"

    #     # Load Hole URDF
    #     hole_xml = ''
    #     with open (model_path + "hole/hole.urdf", "r") as hole_file:
    #         hole_xml=hole_file.read().replace('\n', '')

    #     # Spawn Hole URDF
    #     rospy.wait_for_service('/gazebo/spawn_urdf_model')
    #     try:
    #         spawn_urdf = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
    #         resp_urdf = spawn_urdf("hole", hole_xml, "/", hole_pose, "world")
    #     except rospy.ServiceException, e:
    #         rospy.logerr("Spawn URDF service call failed: {0}".format(e))



    def control(self):
        if self.controller == "joystick":
            self.demo.joystick_control()
        elif self.controller == "falcon":
            self.demo.falcon_control()

    def run(self):

        while not rospy.is_shutdown():


            if self.demo.falcon._button == 1 and self.record == False:
                self.record = True
                self.demo.finished = False
                print "starting record"
                # one time record
                rospy.wait_for_service('start_recording')
                try:
                    start_recording = rospy.ServiceProxy('start_recording', StartRecording)
                    resp1 = start_recording()
                    print "started"
                except rospy.ServiceException, e:
                    print "Service call failed: %s"%e

            while self.record:

                self.control() # get the target position to reach

                if self.demo._limb.ik_request(self.demo._pose, end_point = 'right_gripper_tip') != False:

                    self.demo._limb.set_joint_positions(self.demo._limb.ik_request(self.demo._pose, end_point = 'right_gripper_tip'))

                else:
                    print "IK Request failed."

                # print self.robot_hand_z
                # if self.robot_hand_z <= 0.8206:

                if self.demo.finished:
                    rospy.wait_for_service('stop_recording')
                    try:
                        stop_recording = rospy.ServiceProxy('stop_recording', StopRecording)
                        resp1 = stop_recording()
                        print "stopped"
                        self.record = False
                    except rospy.ServiceException, e:
                        print "Service call failed: %s"%e
                    self.demo.reset_point()
                    self.demo.move_to_neutral()

            # to go back
            # self.control() # get the target position to reach
            # print "One demo done."
            # if self.demo._limb.ik_request(self.demo._pose) != False:

            #     self.demo._limb.set_joint_positions(self.demo._limb.ik_request(self.demo._pose))

            # else:
            #     print "IK Request failed."


        print "outside is_shutdown"
