#!/usr/bin/env python

# the script to generate expert demonstrations

import argparse

import rospy

import intera_interface
import rospkg

from std_msgs.msg import String
from std_msgs.msg import Bool

from gazebo_msgs.srv import (
    SpawnModel,
    DeleteModel,
)
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from intera_core_msgs.msg import EndpointState

from intera_interface import CHECK_VERSION

import time
import datetime
import sys

from copy import deepcopy
import numpy as np
import csv
import math

import random
from utils import CustomController
from utils import NovintFalcon
from sawyer_demonstration.srv import *
from ros_falcon.msg import falconForces
from gazebo_msgs.msg import ContactsState
from geometry_msgs.msg import Point
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Quaternion

from gazebo_msgs.msg import ModelStates
import scipy.signal as signal
import copy


class Demonstration(object):
    def __init__(self, task):

        '''self._contact_data_file = open('/home/nilesh/san/sawyer_ws_3/src/sawyer-robot-learning/sawyer_demonstration/scripts/data.csv', 'w')
        self._csv_writer = csv.writer(self._contact_data_file)
        self._csv_writer.writerow(['z'])'''

        self._limb = intera_interface.Limb('right')
        self.joints_right = self._limb.joint_names()
        self._task = task
        self._reset_point = [0.4401, 0.0142, -0.2045]
        self.radius = 0.040
        #0.4003 0.0000
        #self._reset_point = [0.4, 0.0, -0.05]

        self._pose, self._point, self._q = self._set_initial_pose_of_eef()
        tmp = [-0.38999412850956094, -0.575235548734848, 0.002277698221086588, 2.141809310862694, -0.36427095792419095, 0.004169374681665339, 3.1154025545442323]
        self.init_angles = {}
        for i, j in enumerate(self.joints_right):
            self.init_angles[j] = tmp[i]

        self._current_x = 0.0
        self._current_y = 0.0
        self._current_z = 0.0
        self._is_joystick = False
        self._is_falcon = True
        # self.joystick = CustomController()
        self.falcon = NovintFalcon()
        self._initial_setup()


        self.collision_data_buffer_in = []
        self.collision_data_buffer_out = []
        self.old_pose = 0.0
        self.new_pose = 0.0
        self.collided = False
        self.finished = False

        if self._task == "pih":
            self.peg_publisher = rospy.Publisher('/peg_position', Point, queue_size=10)
            # rospy.Subscriber('/right_gripper_r_finger_contact_sensor_state', ContactsState, self._right_gripper_cb)
            # rospy.Subscriber('/peg_ft_contact_sensor_state', ContactsState, self._peg_ft_cb)
            rospy.Subscriber('/peg_contact_sensor_state', ContactsState, self._peg_contact_sensor_cb)
            rospy.Subscriber('/hole_contact_sensor_state', ContactsState, self._hole_contact_sensor_cb)
            # rospy.Subscriber('/hole_contact_sensor_new_state', ContactsState, self._hole_contact_sensor_cb)
        # rospy.Subscriber('/right_gripper_r_finger_contact_sensor_state', ContactsState, self._right_gripper_cb)
        # rospy.Subscriber('/peg_contact_sensor_state', ContactsState, self._peg_contact_sensor_cb)
        rospy.Subscriber('/robot/limb/right/endpnt_state', EndpointState,self._end_point_state_cb)




    def _initial_setup(self):

        self.move_to_neutral()

        self._spawn_table()
        self._spawn_hole()
        # self._spawn_hole_large()

        rospy.on_shutdown(self._delete_table)
        rospy.on_shutdown(self._delete_hole)
        # rospy.on_shutdown(self._delete_hole_large)


    def _set_initial_pose_of_eef(self):
        # End effector orientation is fixed.
        q = Quaternion()
        q.x = 0.642161760993
        q.y = 0.766569045326
        q.z = 0.000271775440016
        q.w = 0.00031241683589

        point = Point()
        pose = Pose()

        point.x = self._reset_point[0]
        point.y = self._reset_point[1]
        point.z = self._reset_point[2]

        pose.position = point
        pose.orientation = q

        return pose, point, q



    def _spawn_table(self, table_pose=Pose(position=Point(x=0.7, y=0.0, z=0.00)),table_reference_frame="world"):
        # Get Models' Path
        model_path = rospkg.RosPack().get_path('sawyer_gazebo_env')+"/models/"
        # Load Table URDF
        table_xml = ''
        with open (model_path + "table/table.urdf", "r") as table_file:
            table_xml=table_file.read().replace('\n', '')
        # Spawn Table URDF
        rospy.wait_for_service('/gazebo/spawn_urdf_model')
        try:
            spawn_urdf = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
            resp_urdf = spawn_urdf("table", table_xml, "/",
                                 table_pose, table_reference_frame)
        except rospy.ServiceException, e:
            rospy.logerr("Spawn SDF service call failed: {0}".format(e))


    # def spawn_saucer(_x, _y, saucer_reference_frame="world"):
    #     # block_pose=Pose(position=Point(x=0.4225, y=0.1265, z=0.7725))
    #     saucer_pose=Pose(position=Point(x=_x, y=_y, z=0.825))
    #     # Get Models' Path
    #     model_path = rospkg.RosPack().get_path('sawyer_gazebo_env')+"/models/"

    #     # Load Saucer URDF
    #     saucer_xml = ''
    #     with open (model_path + "plate/plate.urdf", "r") as saucer_file:
    #         saucer_xml=saucer_file.read().replace('\n', '')

    #     # Spawn Saucer URDF
    #     rospy.wait_for_service('/gazebo/spawn_urdf_model')
    #     try:
    #         spawn_urdf = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
    #         resp_urdf = spawn_urdf("plate", saucer_xml, "/",
    #                                saucer_pose, "world")
    #     except rospy.ServiceException, e:
    #         rospy.logerr("Spawn URDF service call failed: {0}".format(e))


#--------------------------------------------------------------------------------------------#
    def _spawn_hole(self):
        hole_pose=Pose(position=Point(x=-0.2, y=0.0, z=0.0))
        # Get Models' Path
        model_path = rospkg.RosPack().get_path('sawyer_gazebo_env')+"/models/"

        # Load Hole URDF
        hole_xml = ''
        with open (model_path + "hole2/hole.urdf", "r") as hole_file:
            hole_xml=hole_file.read().replace('\n', '')

        # Spawn Hole URDF
        rospy.wait_for_service('/gazebo/spawn_urdf_model')
        try:
            spawn_urdf = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
            resp_urdf = spawn_urdf("hole", hole_xml, "/", hole_pose, "world")
        except rospy.ServiceException, e:
            rospy.logerr("Spawn URDF service call failed: {0}".format(e))
#---------------------------------------------------------------------------------------------#
    def _delete_hole(self):
        try:
            delete_model = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)
            resp_delete = delete_model("hole")
        except rospy.ServiceException, e:
            print("Delete Model service call failed: {0}".format(e))

#---------------------------------------------------------------------------------------------#

#     def _spawn_hole_large(self):
#         hole_large_pose=Pose(position=Point(x=-0.1, y=0.0, z=0.0))
#         # Get Models' Path
#         model_path = rospkg.RosPack().get_path('sawyer_gazebo_env')+"/models/"

#         # Load Hole URDF
#         hole_large_xml = ''
#         with open (model_path + "hole_large/hole_large.urdf", "r") as hole_large_file:
#             hole_large_xml=hole_large_file.read().replace('\n', '')

#         # Spawn Hole URDF
#         rospy.wait_for_service('/gazebo/spawn_urdf_model')
#         try:
#             spawn_urdf = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
#             resp_urdf = spawn_urdf("hole_large", hole_large_xml, "/", hole_large_pose, "world")
#         except rospy.ServiceException, e:
#             rospy.logerr("Spawn URDF service call failed: {0}".format(e))
# #---------------------------------------------------------------------------------------------#
#     def _delete_hole_large(self):
#         try:
#             delete_model = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)
#             resp_delete = delete_model("hole_large")
#         except rospy.ServiceException, e:
#             print("Delete Model service call failed: {0}".format(e))

#---------------------------------------------------------------------------------------------#

    def _delete_table(self):
        try:
            delete_model = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)
            resp_delete = delete_model("table")
        except rospy.ServiceException, e:
            print("Delete Model service call failed: {0}".format(e))

    def _end_point_state_cb(self, data):
        self._current_x = data.pose.position.x
        self._current_y = data.pose.position.y
        self._current_z = data.pose.position.z

    # def _right_gripper_cb(self, data):
    def _peg_contact_sensor_cb(self, data):
        if data.states:
            # self.collided = True
            force_obj = falconForces()
            force_obj.X = 0.0
            force_obj.Y = 0.0
            force_obj.Z = 0.0
            self.pos_z = self._limb.tip_state('right_gripper_tip').pose.position.z
            # collision for the first time
            if not self.collided:
                self.old_pose = self._limb.tip_state('right_gripper_tip').pose.position.z
                #self.new_pose = self.falcon._value_y
            self.collided = True
            # print self.old_pose - self.new_pose
            diff_force = -100.0*(self.old_pose - self.pos_z) # Fz = k*dz, it considers the point of contact as reference.
            force_obj.Z = diff_force # -ve of -ve force
            # print "Force =", force_obj.Z
            #self.new_pose = self.falcon._value_y


            # if len(self.collision_data_buffer_in) <= 100:
            #     self.collision_data_buffer_in.append(force_obj.Z)
            # else:
            #     out = signal.savgol_filter(self.collision_data_buffer_in, 51, 3)
            #     print out
            #     self.collision_data_buffer_in = []
            #     force_obj.Z = -(sum(out)/len(out))
            #     self.falcon._pub.publish(force_obj)
            ##############################################
            self.falcon._pub.publish(force_obj) #for feedback!
            ##############################################
        else:
            self.collided = False

    # def _peg_ft_cb(self, data):
    def _peg_ft_cb(self, data):
        if data.states:
            self._csv_writer.writerow([data.states[1].total_wrench.force.z])

            force_obj = falconForces()
            force_obj.X = 0.0
            force_obj.Y = 0.0
            force_obj.Z = 0.63 - self._point_peg.z
            # force_obj.X = data.states[1].total_wrench.force.x
            # force_obj.Y = data.states[1].total_wrench.force.y
            # force_obj.Z = data.states[1].total_wrench.force.z
            # print force_obj.Z
            # self.falcon._pub.publish(force_obj)
#-------------------------------------------------------------------------------------------------#

    # def _hole_contact_sensor_cb(self, data):      #callback for the hole's contact sensor
    #     if data.states:
    #         print "yay touch"
    #         self.collided = True
    #         hole_contact = ContactsState()

#-------------------------------------------------------------------------------------------------#

    def _hole_contact_sensor_cb(self, data):      #callback for the hole_large contact sensor
        if data.states:
            if not self.finished:
                print "yay touch"
            #print(self._limb.tip_state('right_gripper_tip').pose.position)
            self.finished = True
            hole_new_contact = ContactsState()

#-------------------------------------------------------------------------------------------------#
    def _servo_to_pose(self, pose, _tip_name, time=1.0, steps=100.0):
        ''' An *incredibly simple* linearly-interpolated Cartesian move '''
        r = rospy.Rate(1/(time/steps)) # Defaults to 100Hz command rate
        current_pose = self._limb.endpoint_pose()
        ik_delta = Pose()
        ik_delta.position.x = (current_pose['position'].x - pose.position.x) / steps
        ik_delta.position.y = (current_pose['position'].y - pose.position.y) / steps
        ik_delta.position.z = (current_pose['position'].z - pose.position.z) / steps
        ik_delta.orientation.x = (current_pose['orientation'].x - pose.orientation.x) / steps
        ik_delta.orientation.y = (current_pose['orientation'].y - pose.orientation.y) / steps
        ik_delta.orientation.z = (current_pose['orientation'].z - pose.orientation.z) / steps
        ik_delta.orientation.w = (current_pose['orientation'].w - pose.orientation.w) / steps
        for d in range(int(steps), -1, -1):
            if rospy.is_shutdown():
                return
            ik_step = Pose()
            ik_step.position.x = d*ik_delta.position.x + pose.position.x
            ik_step.position.y = d*ik_delta.position.y + pose.position.y
            ik_step.position.z = d*ik_delta.position.z + pose.position.z
            ik_step.orientation.x = d*ik_delta.orientation.x + pose.orientation.x
            ik_step.orientation.y = d*ik_delta.orientation.y + pose.orientation.y
            ik_step.orientation.z = d*ik_delta.orientation.z + pose.orientation.z
            ik_step.orientation.w = d*ik_delta.orientation.w + pose.orientation.w
            joint_angles = self._limb.ik_request(ik_step, _tip_name)
            if joint_angles:
                self._limb.move_to_joint_positions(joint_angles, timeout = 1.0/(time/steps))
            else:
                rospy.logerr("No Joint Angles provided for move_to_joint_positions. Staying put.")
            #r.sleep()
        rospy.sleep(1.0)

    def reset_point(self):
        yy = (random.random()*2*self.radius) - self.radius
        xx = math.sqrt(self.radius*self.radius - yy*yy)
        if random.random() > 0.5:
            xx = -xx

        self._point.x = xx + 0.4003
        self._point.y = yy + 0.0000
        self._point.z = self._reset_point[2]

    def move_to_neutral(self):

        # -J sawyer::right_j0 -0.27
        # -J sawyer::right_j1 1.05
        # -J sawyer::right_j2 0.00
        # -J sawyer::right_j3 0.49
        # -J sawyer::right_j4 -0.08
        # -J sawyer::right_j5 -0.06
        # -J sawyer::right_j6 0.027
        # -J sawyer::head_pan 0.00
        print("Moving To Neutral....")
        tmp = {'right_j6': 2.6341108402025846, 'right_j5': -0.09881254041094753, 'right_j4': 0.09323309331150138, 'right_j3': 2.3524184835294513, 'right_j2': 0.012295990854670754, 'right_j1': -0.6839585138100157, 'right_j0': -0.4208275745414778}


        self._limb.move_to_joint_positions(tmp)

        self._servo_to_pose(self._pose, 'right_gripper_tip')


    def joystick_control(self):

        if len(self.joystick._controls) > 0:
            # if lefstBumper button is clicked : Open the gripper.
            if self.joystick._controls['leftBumper'] and (not self.joystick.gripper_open):
                self.joystick.gripper.open()
                self.joystick.gripper_open = True
                time.sleep(0.05)
            if self.joystick._controls['leftBumper'] and self.joystick.gripper_open:
                self.joystick.gripper.close()
                self.joystick.gripper_open = False
                time.sleep(0.05)

            if self.joystick._controls['rightBumper']:
                # reset()
                self.move_to_neutral()
                print "reset"

            if self.joystick._controls['leftStickHorz'] < 0.0:
                self._point.y -= 0.005
                # print "right"
            elif self.joystick._controls['leftStickHorz'] > 0.0:
                self._point.y += 0.005
                # print "left"

            if self.joystick._controls['leftStickVert'] < 0.0:
                self._point.x -= 0.005
                # print "down"
            elif self.joystick._controls['leftStickVert'] > 0.0:
                self._point.x += 0.005
                # print "up"

            if self.joystick._controls['rightStickVert'] > 0.0:
                self._point.z -= 0.005
            elif self.joystick._controls['rightStickVert'] < 0.0:
                self._point.z += 0.005

    def falcon_control(self):
        if self.falcon._button == 4:

            # if self._task == "reaching":
            # print round(self.falcon._value_x, 3), " ", round(self.falcon._value_y, 3), " ", round(self.falcon._value_z, 3)
            xx = round(self.falcon._value_x, 6)
            yy = round(self.falcon._value_y, 6)
            zz = round(self.falcon._value_z, 6)
            eef = self._limb.tip_state('right_gripper_tip').pose.position
            #'''
            self._point.z = eef.z
            if self.falcon._button == 2:
                self._point.z = eef.z - 0.005
            self._point.y = eef.y + (xx - self.falcon._current_value_x)/0.05
            self._point.x = eef.x + (zz - self.falcon._current_value_z)/0.05

            self.falcon._current_value_x = xx
            self.falcon._current_value_y = yy
            self.falcon._current_value_z = zz
            '''
            self._point.z = eef.z + round(yy/40,3)
            self._point.y = eef.y + round(xx/40,3)
            self._point.x = eef.x + round(zz/40,3)
            #'''

            '''self._point.z += round(self.falcon._value_y, 3)/30.0
            self._point.y += round(self.falcon._value_x, 3)/30.0

            if round(self.falcon._value_z, 3)/30.0 < self.falcon._current_value_z:
                self._point.x -= round(self.falcon._value_z, 3)/30.0
                # point.x -= abs(falcon._current_value_z - (round(falcon._value_z, 3)/10.0))
            elif round(self.falcon._value_z, 3)/30.0 > self.falcon._current_value_z:
                self._point.x += round(self.falcon._value_z, 3)/30.0
                # point.x += abs(falcon._current_value_z - (round(falcon._value_z, 3)/10.0))

            self.falcon._current_value_z = round(self.falcon._value_z, 3)/30.0'''
            #########################################################################################
            # elif self._task == "pih":
            #     # print round(self.falcon._value_x, 3), " ", round(self.falcon._value_y, 3), " ", round(self.falcon._value_z, 3)
            #     self._point_peg.z += round(self.falcon._value_y, 3)/500.0
            #     self._point_peg.y += round(self.falcon._value_x, 3)/500.0

            #     if round(self.falcon._value_z, 3)/500.0 < self.falcon._current_value_z:
            #         self._point_peg.x -= round(self.falcon._value_z, 3)/500.0
            #         # point.x -= abs(falcon._current_value_z - (round(falcon._value_z, 3)/10.0))
            #     elif round(self.falcon._value_z, 3)/500.0 > self.falcon._current_value_z:
            #         self._point_peg.x += round(self.falcon._value_z, 3)/500.0
            #         # point.x += abs(falcon._current_value_z - (round(falcon._value_z, 3)/10.0))

            #     self.falcon._current_value_z = round(self.falcon._value_z, 3)/500.0
        elif self.falcon._button == 2:
            eef = self._limb.tip_state('right_gripper_tip').pose.position
            self._point.z = eef.z - 0.01
        else:
            xx = round(self.falcon._value_x, 6)
            yy = round(self.falcon._value_y, 6)
            zz = round(self.falcon._value_z, 6)

            self.falcon._current_value_x = xx
            self.falcon._current_value_y = yy
            self.falcon._current_value_z = zz


    def replay(self, file_name):

        def try_float(x):
            try:
                return float(x)
            except ValueError:
                return None

        src_file = open(file_name, 'rb')
        reader = csv.reader(src_file)
        reader.next()

        for row in reader:
            j0 = try_float(row[1])
            j1 = try_float(row[2])
            j2 = try_float(row[3])
            j3 = try_float(row[4])
            j4 = try_float(row[5])
            j5 = try_float(row[6])
            j6 = try_float(row[7])
            joint_positions = [j0, j1, j2, j3, j4, j5, j6]
            limb.set_joint_positions(joint_positions)
