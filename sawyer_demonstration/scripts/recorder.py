import rospy

import intera_interface

import rospy

import intera_interface

from intera_interface import CHECK_VERSION

from std_msgs.msg import String
from std_msgs.msg import Bool
from sensor_msgs.msg import Image
from gazebo_msgs.msg import ContactsState
# from gazebo_msgs.msg import falconForces
from gazebo_msgs.msg import ModelStates
from cv_bridge import CvBridge, CvBridgeError
import cv2
import os

from sawyer_demonstration.srv import *

class DataRecorder(object):
    def __init__(self, rate, side="right"):
        """
        Records joint data to a file at a specified rate.
        """
        # self._raw_rate = rate
        self._rate = rospy.Rate(rate)
        self._start_time = rospy.get_time()
        self._done = True
        self.record_last = False
        self.counter = 0
        self.is_recording = False
        self.filename = str(self.counter)+'.csv'
        self.cv2_img = None
        self.img_counter = 0

        # self.cube_pose_x = 0.0
        # self.cube_pose_y = 0.0

        self.last_eef_pose_x = 0.0
        self.last_eef_forc_x = 0.0
        self.last_eef_forc_y = 0.0
        self.last_eef_forc_z = 0.0

        self.force_obj = 0.0
        self.hole_contact = 0.0
        # self.haptic_feedback = 0.0


        self._limb_right = intera_interface.Limb(side)
        joints_right = self._limb_right.joint_names()           # need for joint angles
        data_path = 'data_pose_2/'
        if not os.path.exists(data_path):
            os.makedirs(data_path)

        image_path = 'data_pose_2/'
        if not os.path.exists(image_path):
            os.makedirs(image_path)

        self.ep_num = 0
        with open('data_pose_2/'+self.filename, 'w') as f:
            f.write('counter,')
            f.write('time,')
            # f.write('cube_pose_x,')
            # f.write('cube_pose_y,')
            f.write(','.join([j for j in joints_right]) + ',')  # joint angles
            f.write('j0_vel,')      #joint-velocities
            f.write('j1_vel,')
            f.write('j2_vel,')
            f.write('j3_vel,')
            f.write('j4_vel,')
            f.write('j5_vel,')
            f.write('j6_vel,')
            f.write('j0_tor,')      #joint-torques
            f.write('j1_tor,')
            f.write('j2_tor,')
            f.write('j3_tor,')
            f.write('j4_tor,')
            f.write('j5_tor,')
            f.write('j6_tor,')
            f.write('eef_pose_x,')  #end-effector position - x
            f.write('eef_pose_y,')  #end-effector position - y
            f.write('eef_pose_z,')  #end-effector position - z
            f.write('eef_o_x,')     #end-effector orientation
            f.write('eef_o_y,')
            f.write('eef_o_z,')
            f.write('eef_o_w,')
            f.write('eef_vel_x,')   #end-effector velocities
            f.write('eef_vel_y,')
            f.write('eef_vel_z,')
            # f.write('tip_p_x,')
            # f.write('tip_p_y,')
            # f.write('tip_p_z,')
            # f.write('tip_o_x,')
            # f.write('tip_o_y,')
            # f.write('tip_o_z,')
            # f.write('tip_o_w,')
            # f.write('tip_v_x,')
            # f.write('tip_v_y,')
            # f.write('tip_v_z,')

            # f.write('eef_forc_x,')  #end-effector forces
            # f.write('eef_forc_y,')
            # f.write('eef_forc_z,')
            f.write('force_obj,')
            f.write('hole_contact,')
            # f.write('haptic_feedback,')
            f.write('\n')

    def _time_stamp(self):
        return rospy.get_time() - self._start_time

    def start(self):
        self._done =  False

    def stop(self):
        self._done = True
        self.record_last = True

    # def record(self):
    #     """
    #     Records the current joint positions to a csv file if outputFilename was
    #     provided at construction this function will record the latest set of
    #     joint angles in a csv format.

    #     """
    #     joints_right = self._limb_right.joint_names()

    #     data_path = 'data/'
    #     if not os.path.exists(data_path):
    #         os.makedirs(data_path)

    #     image_path = 'data/'+str(self.counter)
    #     if not os.path.exists(image_path):
    #         os.makedirs(image_path)

    #     with open('data/'+self.filename, 'w') as f:
    #         f.write('time,')
    #         temp_str = '' if self._gripper else '\n'
    #         f.write(','.join([j for j in joints_right]) + ',' + temp_str)
    #         f.write('joint_trajectory_id ,')
    #         if self._gripper:
    #             f.write(self.gripper_name+'\n')
    #         while not dr._done:
    #             if self._gripper:
    #                 if self._cuff.upper_button():
    #                     self._gripper.open()
    #                 elif self._cuff.lower_button():
    #                     self._gripper.close()
    #             angles_right = [self._limb_right.joint_angle(j)
    #                             for j in joints_right]
    #             f.write("%f," % (self._time_stamp(),))
    #             f.write(','.join([str(x) for x in angles_right]) + ',' + temp_str)

    #             # save current snapshot
    #             cv2.imwrite('data/'+str(dr.counter)+'/image_'+str(self.img_counter)+'.jpeg', dr.cv2_img)

    #             f.write('data_jt_'+str(self.img_counter)+',')
    #             self.img_counter += 1

    #             if self._gripper:
    #                 f.write(str(self._gripper.get_position()) + '\n')

    #             self._rate.sleep()

    #         self.img_counter = 0

    def save_to_file(self, f, eef_pose, joints_right, peg_contact, hole_contact):
        eef_pose_x = eef_pose.x
        eef_pose_y = eef_pose.y
        eef_pose_z = eef_pose.z

        eef_orient = self._limb_right.endpoint_pose()['orientation']
        eef_o_x = eef_orient.x
        eef_o_y = eef_orient.y
        eef_o_z = eef_orient.z
        eef_o_w = eef_orient.w


        eef_vel = self._limb_right.endpoint_velocity()['linear']
        eef_vel_x = eef_vel.x
        eef_vel_y = eef_vel.y
        eef_vel_z = eef_vel.z

        eef_forc = self._limb_right.endpoint_effort()['force'] #end_point effort - force
        eef_forc_x = eef_forc.x
        eef_forc_y = eef_forc.y
        eef_forc_z = eef_forc.z

        # tip_state = self._limb_right.tip_state('right_gripper_tip')
        # tip_p_x = float(tip_state.pose.position.x)
        # tip_p_y = float(tip_state.pose.position.y)
        # tip_p_z = float(tip_state.pose.position.z)
        # tip_o_x = float(tip_state.pose.orientation.x)
        # tip_o_y = float(tip_state.pose.orientation.y)
        # tip_o_z = float(tip_state.pose.orientation.z)
        # tip_o_w = float(tip_state.pose.orientation.w)
        # tip_v_x = float(tip_state.twist.linear.x)
        # tip_v_y = float(tip_state.twist.linear.y)
        # tip_v_z = float(tip_state.twist.linear.z)



        f.write(str(self.img_counter)+',')
        f.write("%f," % (self._time_stamp(),))

        # f.write(str(self.cube_pose_x)+',')
        # f.write(str(self.cube_pose_y)+',')

        angles_right = [self._limb_right.joint_angle(j) for j in joints_right] #need for joint angles
        f.write(','.join([str(x) for x in angles_right]) + ',')                #need for joint angles

        velocities_right = [self._limb_right.joint_velocity(j) for j in joints_right] #need for joint velocities
        f.write(','.join([str(x) for x in velocities_right]) + ',')            #need for joint velocities

        effort_right = [self._limb_right.joint_effort(j) for j in joints_right] #need for joint torques
        f.write(','.join([str(x) for x in effort_right]) + ',')            #need for joint torques

        f.write(str(eef_pose_x)+',')
        f.write(str(eef_pose_y)+',')
        f.write(str(eef_pose_z)+',')

        f.write(str(eef_o_x)+',')
        f.write(str(eef_o_y)+',')
        f.write(str(eef_o_z)+',')
        f.write(str(eef_o_w)+',')

        f.write(str(eef_vel_x)+',')
        f.write(str(eef_vel_y)+',')
        f.write(str(eef_vel_z)+',')

        '''f.write(str(eef_forc_x)+',')
        f.write(str(eef_forc_y)+',')
        f.write(str(eef_forc_z)+',')'''

        # f.write(str(tip_p_x)+',')
        # f.write(str(tip_p_y)+',')
        # f.write(str(tip_p_z)+',')
        # f.write(str(tip_o_x)+',')
        # f.write(str(tip_o_y)+',')
        # f.write(str(tip_o_z)+',')
        # f.write(str(tip_o_w)+',')
        # f.write(str(tip_v_x)+',')
        # f.write(str(tip_v_y)+',')
        # f.write(str(tip_v_z)+',')

        f.write(str(peg_contact)+',')
        f.write(str(hole_contact)+',')
        # f.write(str(self.haptic_feedback)+',')

        f.write('\n')

        # save current snapshot
        cv2.imwrite('data_pose_2/'+str(self.img_counter)+'.jpeg', dr.cv2_img)

        self.img_counter += 1
        return eef_pose_x

    def record(self):
        ## for recording images and corresponding
        ## position labels. Supposed to be a very
        ## simple task

        joints_right = self._limb_right.joint_names()


        with open('data_pose_2/'+self.filename, 'a') as f:
            # f.write('time,')
            # temp_str = '' if self._gripper else '\n'
            # f.write(','.join([j for j in joints_right]) + ',' + temp_str)
            # if self._gripper:
            #     f.write(self.gripper_name+'\n')
            while not dr._done:


                eef_pose = self._limb_right.endpoint_pose()['position']

                if eef_pose.x != self.last_eef_pose_x: #to avoid redundant data when starting new demo
                    eef_pose_x = self.save_to_file(f, eef_pose, joints_right, self.force_obj, self.hole_contact)

                    self.last_eef_pose_x = eef_pose_x
                    # if self.hole_contact = 1.0:
                    #     break

                self._rate.sleep()

            if self.record_last:
                self.ep_num += 1
                print("Completed end episode", self.ep_num)
                eef_pose = self._limb_right.endpoint_pose()['position']
                eef_pose_x = self.save_to_file(f, eef_pose, joints_right, 1, 1)

                self.last_eef_pose_x = eef_pose_x

            self.record_last = False



def handle_start_recording(req):
    # print "returning start"
    dr.start()
    return StartRecordingResponse()

def handle_stop_recording(req):
    # print "returning stop"
    dr.stop()
    while(dr.record_last):
        a = 1
    # dr.counter += 1
    # dr.filename = str(dr.counter)+'.csv'
    return StopRecordingResponse()

def handle_image_cb(msg):
    try:
        # Convert your ROS Image message to OpenCV2
        dr.cv2_img = bridge.imgmsg_to_cv2(msg, "bgr8")
    except CvBridgeError, e:
        print(e)

def handle_peg_contact_sensor_cb(msg):
    if msg.states:
        dr.force_obj = 1.0 #msg.states[0].total_wrench.force.z
    else:
        dr.force_obj = 0.0

def handle_hole_contact_sensor_cb(msg):
    if msg.states:
        dr.hole_contact = 1.0
    else:
        dr.hole_contact = 0.0

# def handle_haptic_feedback_cb(msg):
#     if msg.states:
#         dr.haptic_feedback = 1.0
#     else:
#         dr.haptic_feedback = 0.0

        # dr.cube_pose_x = round(msg.pose[4].position.x, 2)
        # dr.cube_pose_y = round(msg.pose[4].position.y, 2)

# def handle_model_states_cb(msg):
#     if "block" in msg.name:
#         dr.cube_pose_x = round(msg.pose[4].position.x, 2)
#         dr.cube_pose_y = round(msg.pose[4].position.y, 2)


rospy.init_node('joint_recorder_node')
# rospy.init_node('joint_velocities_recorder_node')

# Instantiate CvBridge
bridge = CvBridge()

dr = DataRecorder(10)
s_start = rospy.Service('start_recording', StartRecording, handle_start_recording)
s_stop = rospy.Service('stop_recording', StopRecording, handle_stop_recording)
camera_sub = rospy.Subscriber('/front_camera/camera/image_raw', Image, handle_image_cb)
peg_force_sub = rospy.Subscriber('/peg_contact_sensor_state', ContactsState, handle_peg_contact_sensor_cb)
hole_contact_sub = rospy.Subscriber('/hole_contact_sensor_state', ContactsState, handle_hole_contact_sensor_cb)
# haptic_feedback_sub = rospy.Subscriber('/falconForce', falconForces, handle_haptic_feedback_cb)

# cube_pose_sub = rospy.Subscriber('/gazebo/model_states', ModelStates, handle_model_states_cb)

while not rospy.is_shutdown():
    dr.record()
