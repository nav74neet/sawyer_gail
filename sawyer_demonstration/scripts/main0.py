#!/usr/bin/env python

import rospy
import argparse
from peg_in_hole import PegInHole


import intera_interface
import rospkg

from std_msgs.msg import String
from std_msgs.msg import Bool

from gazebo_msgs.srv import (
    SpawnModel,
    DeleteModel,
)
from geometry_msgs.msg import (
    PoseStamped,
    Pose,
    Point,
    Quaternion,
)
from intera_core_msgs.msg import EndpointState

from intera_interface import CHECK_VERSION

import time
import datetime
import sys

from copy import deepcopy
import numpy as np
import csv

import random
from geometry_msgs.msg import Point
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Quaternion

from gazebo_msgs.msg import ModelStates
from ros_falcon.msg import falconForces
import scipy.signal as signal

class Task(object):
    def __init__(self, task):
        self._limb = intera_interface.Limb('right')
        self._initial_setup()


    def _initial_setup(self):

        tmp = {'right_j6': 2.6341108402025846, 'right_j5': -0.09881254041094753, 'right_j4': 0.09323309331150138, 'right_j3': 2.3524184835294513, 'right_j2': 0.012295990854670754, 'right_j1': -0.6839585138100157, 'right_j0': -0.4208275745414778}

        self._limb.move_to_joint_positions(tmp)
        self._spawn_table()
        self._spawn_hole()
        rospy.on_shutdown(self._delete_table)
        rospy.on_shutdown(self._delete_hole)




    def _spawn_table(self, table_pose=Pose(position=Point(x=0.7, y=0.0, z=0.00)),table_reference_frame="world"):
        # Get Models' Path
        model_path = rospkg.RosPack().get_path('sawyer_gazebo_env')+"/models/"
        # Load Table URDF
        table_xml = ''
        with open (model_path + "table/table.urdf", "r") as table_file:
            table_xml=table_file.read().replace('\n', '')
        # Spawn Table URDF
        rospy.wait_for_service('/gazebo/spawn_urdf_model')
        try:
            spawn_urdf = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
            resp_urdf = spawn_urdf("table", table_xml, "/",
                                 table_pose, table_reference_frame)
        except rospy.ServiceException, e:
            rospy.logerr("Spawn SDF service call failed: {0}".format(e))


    # def spawn_saucer(_x, _y, saucer_reference_frame="world"):
    #     # block_pose=Pose(position=Point(x=0.4225, y=0.1265, z=0.7725))
    #     saucer_pose=Pose(position=Point(x=_x, y=_y, z=0.825))
    #     # Get Models' Path
    #     model_path = rospkg.RosPack().get_path('sawyer_gazebo_env')+"/models/"

    #     # Load Saucer URDF
    #     saucer_xml = ''
    #     with open (model_path + "plate/plate.urdf", "r") as saucer_file:
    #         saucer_xml=saucer_file.read().replace('\n', '')

    #     # Spawn Saucer URDF
    #     rospy.wait_for_service('/gazebo/spawn_urdf_model')
    #     try:
    #         spawn_urdf = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
    #         resp_urdf = spawn_urdf("plate", saucer_xml, "/",
    #                                saucer_pose, "world")
    #     except rospy.ServiceException, e:
    #         rospy.logerr("Spawn URDF service call failed: {0}".format(e))


#--------------------------------------------------------------------------------------------#
    def _spawn_hole(self):
        hole_pose=Pose(position=Point(x=-0.2, y=0.0, z=0.0))
        # Get Models' Path
        model_path = rospkg.RosPack().get_path('sawyer_gazebo_env')+"/models/"

        # Load Hole URDF
        hole_xml = ''
        with open (model_path + "hole2/hole.urdf", "r") as hole_file:
            hole_xml=hole_file.read().replace('\n', '')

        # Spawn Hole URDF
        rospy.wait_for_service('/gazebo/spawn_urdf_model')
        try:
            spawn_urdf = rospy.ServiceProxy('/gazebo/spawn_urdf_model', SpawnModel)
            resp_urdf = spawn_urdf("hole", hole_xml, "/", hole_pose, "world")
        except rospy.ServiceException, e:
            rospy.logerr("Spawn URDF service call failed: {0}".format(e))
#---------------------------------------------------------------------------------------------#
    def _delete_hole(self):
        try:
            delete_model = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)
            resp_delete = delete_model("hole")
        except rospy.ServiceException, e:
            print("Delete Model service call failed: {0}".format(e))

#---------------------------------------------------------------------------------------------#

    def _delete_table(self):
        try:
            delete_model = rospy.ServiceProxy('/gazebo/delete_model', DeleteModel)
            resp_delete = delete_model("table")
        except rospy.ServiceException, e:
            print("Delete Model service call failed: {0}".format(e))

def main0():
    rospy.init_node('pih_node')

    epilog = """
    Usage : roslaunch sawyer_demonstration sawyer_demonstration.launch \
    task:=reaching controller:=falcon

    """
    time.sleep(10)
    task = Task("pih")
    while True:
        a=1


if __name__ == '__main__':
    main0()
